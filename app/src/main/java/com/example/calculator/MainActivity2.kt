package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.widget.TextView


class MainActivity2 : AppCompatActivity() {
    lateinit var txtResult : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        txtResult = findViewById(R.id.txt_result)

        var result = intent.getStringExtra("result")
        txtResult.text = result
    }
}